const fs = require('fs');
const { extractPartOfImage } = require('./imageCropper');

function createAnnotateImageRequest(image) {
  return {
    image: { content: image },
    features: [
      {
        maxResults: 50,
        type: 'OBJECT_LOCALIZATION',
      },
      {
        maxResults: 50,
        type: 'WEB_DETECTION',
      },
    ],
  };
}

module.exports = async function vision(ImageBuffer) {
  // Imports the Google Cloud client library
  const vision = require('@google-cloud/vision');

  // Creates a client
  const client = new vision.ImageAnnotatorClient();

  // Performs label detection on the image file
  const [result] = await client.annotateImage({
    image: { content: ImageBuffer },
    features: [
      {
        maxResults: 50,
        type: 'OBJECT_LOCALIZATION',
      },
      {
        maxResults: 50,
        type: 'WEB_DETECTION',
      },
    ],
  });

  const annotations = result.localizedObjectAnnotations;
  const normalizedVertices = annotations.map(ann => ann.boundingPoly.normalizedVertices);
  const croppedBuffers = await Promise.all(
    normalizedVertices.map(async vert => await extractPartOfImage(ImageBuffer, vert))
  );

  const annotateImageRequests = croppedBuffers.map(b => createAnnotateImageRequest(b));

  const [res2] = await client.batchAnnotateImages({ requests: annotateImageRequests });

  res2.responses.map(res => console.log(res.localizedObjectAnnotations));

  return res2.responses.map((e, i, c) => ({
    label: e.webDetection.bestGuessLabels
      .map(guess => guess.label)
      .concat(
        e.webDetection.webEntities
          // .filter(item => item.score > 0.5)
          .sort((itemA, itemB) => (itemA.score > itemB.score ? -1 : 1))
          .filter(item => item.description && item.score)
          .map(item => item.description + ', score: ' + (item.score * 100).toFixed(2) + '%')
      )
      .join('<br>'),
    image: croppedBuffers[i].toString('base64'),
  }));

  // return Promise.all(croppedBuffers).then(bs => [image.toString('base64')].concat(bs.map(b => b.toString('base64'))));

  // const base64CroppedImages = croppedBuffers.map(b => b.toString('base64'));

  // return base64CroppedImages;
};
