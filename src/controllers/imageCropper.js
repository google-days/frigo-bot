const sharp = require('sharp');

function coordsToPixels({ width, height }, googleCoords) {
  const top = Math.round(googleCoords[0].y * height);
  const left = Math.round(googleCoords[0].x * width);

  const calculatedWidth = Math.round(googleCoords[1].x * width - left);
  const calculatedHeight = Math.round(googleCoords[2].y * height - top);

  const coords = {
    width: calculatedWidth,
    height: calculatedHeight,
    left,
    top,
  };

  return coords;
}

async function extractPartOfImage(image, googleCoords) {
  const sharpImage = await sharp(image);
  const { width, height } = await sharpImage.metadata();
  const sharpCoords = await coordsToPixels({ width, height }, googleCoords);

  const crop = await sharpImage.extract(sharpCoords).toBuffer();

  return crop;
}

module.exports = {
  extractPartOfImage,
};
