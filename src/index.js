require('dotenv').config();

if (process.env.GOOGLE_APPLICATION_CREDENTIALS === undefined) {
  throw new Error('WTF DOE JE JONGUH');
}

const path = require('path');

const express = require('express');
const app = express();
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({ storage });

const vision = require('./controllers/vision.controller');

app.get('/', (req, res, err) => {
  res.sendFile(path.resolve('public/index.html'));
});

app.post('/watisdit', upload.single('image'), async (req, res) => {
  console.log(req.file.buffer);
  console.log(req.file);

  const result = await vision(req.file.buffer);

  const images = result.map(b => `<img src="data:image/jpg;base64,${b.image}" /><br>${b.label}<br><br><br>`);

  res.send(images.join(''));
});

app.get('/bier', async (req, res, err) => {
  const result = await vision();

  const images = result.map(b => `<img src="data:image/jpg;base64,${b.image}" /><br>${b.label}<br><br><br>`);

  res.send(images.join(''));
});

app.listen(3000, () => {
  console.log('joe');
});
